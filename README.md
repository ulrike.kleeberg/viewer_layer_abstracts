# viewer_layer_abstracts

now with cool abstract:

- [x] alkalinity.cfg
- [x] chlorophyll.cfg
- [x] oxygen.cfg
- [x] salinity.cfg
- [x] temperature.cfg
- [x] turbidity.cfg
- [x] total_organic_carbon.cfg
- [x] current.cfg
- [ ] pressure.cfg
- [ ] samples.cfg
- [ ] video_images.cfg
- [ ] video_images_2.cfg


